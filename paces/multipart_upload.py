from exploratorium import make_session, remember, timeit
import json
import time


region = 'us-east-1'
profile = 'administrator-47lining-test5'
this = make_session(profile_name=profile, region_name=region)
sqs = this.session.client("sqs", region)
ec2 = this.session.client("ec2", region)
rgt = this.session.client('resourcegroupstaggingapi', region)
s3 = this.session.client('s3', region)


#@remember
def s3_():
    '''common operations
    abort-multipart-upload
    complete-multipart-upload
    create-multipart-upload
    list-multipart-uploads
    list-part-uploads  NO
    list_parts
    upload-part
    upload-part-copy
    put_bucket_lifecycle  deprecated
    put_bucket_lifecycle_configuration
    '''
    verbose = True
    verbose = False

    # list all buckets
    buckets = s3.list_buckets()['Buckets']
    bs = '\n'.join([b['Name'] for b in buckets])
    verbose and print(bs)

    # create a bucket
    bucket_name = 'cm-test-multipart-upload'
    response = s3.create_bucket(Bucket=bucket_name)
    assert response['ResponseMetadata']['HTTPStatusCode'] == 200
    assert response['Location'][1:] == bucket_name
    verbose and print('')

    # put something in the bucket
    response = s3.put_object(Bucket=bucket_name, Key='yoohoo', Body='xyz')
    assert response['ResponseMetadata']['HTTPStatusCode'] == 200
    verbose and print('')

    response = s3.put_object(Bucket=bucket_name, Key='foo', Body='pdq')
    assert response['ResponseMetadata']['HTTPStatusCode'] == 200
    verbose and print('')
            
    # list bucket contents
    response = s3.list_objects(Bucket=bucket_name)
    contents = response['Contents']
    verbose and print('')
    
    # ok.  Above demonstrates that we can create buckets and place ordinary
    # small objects there.
    # 
    # Now for multipart uploads.

    # list multipart uploads
    mp_list = s3.list_multipart_uploads(Bucket=bucket_name)
    assert mp_list['ResponseMetadata']['HTTPStatusCode'] == 200
    verbose and print('')

    # Toss abandoned multipart uploads
    doc = json.loads(open('lifecycle.json').read())
    response = s3.put_bucket_lifecycle_configuration(Bucket=bucket_name,
            LifecycleConfiguration = doc
            )
    assert response['ResponseMetadata']['HTTPStatusCode'] == 200
    # There is no other response expected.
    verbose and print('')

    # Verify the lifecycle policy is in place.
    response = s3.get_bucket_lifecycle(Bucket=bucket_name)
    assert response['ResponseMetadata']['HTTPStatusCode'] == 200

    globals().update(locals())
    return


def multipart_create():
    '''common operations
    abort-multipart-upload
    complete-multipart-upload
    create-multipart-upload
    list-multipart-uploads
    list-part-uploads  NO
    list_parts
    upload-part
    upload-part-copy
    put_bucket_lifecycle  deprecated
    put_bucket_lifecycle_configuration
    '''
    verbose = True
    verbose = False
    # We could have used multipart uploads with client IoT data.
    # Maybe.

    ts = time.time()

    params = dict(
        Bucket = bucket_name,
        Key = 'some/multipart/upload/foo_%s.txt' % ts,
        Metadata = dict(purpose = 'learning',),
        )
    # create

    '''
    response = s3.create_multipart_upload(
        Bucket=Bucket,         # required
        Key=str,               # required
        Metadata=dict,
        Tagging=str,
    )
    '''
    response = s3.create_multipart_upload(**params)
    assert response['ResponseMetadata']['HTTPStatusCode'] == 200
    upload_key = response['Key']
    upload_id = response['UploadId']
    '''
    >>> response['Key']
    'some/multipart/upload/foo_1515763723.2323093.txt'
    >>> response['UploadId']
    'afzjzih7GxIPvt6Vh2Bya_nDTQ03uplSLorUXFdkP.iaZZuPb.h63YAD9zpzJbV_QDcmQFizNeBBh._FKEfq6QmZPSDOa3mPIcovIxz2ik8mS.Nu3DYPxosal5DLTReM1dWJS.myu9y356aJeXqz6S7IjKrPu2roue71KXmHV.o-'

    Parts Upload

    When uploading a part, in addition to the upload ID, you MUST specify a part
    number. You can choose any part number between 1 and 10,000. A part number
    uniquely identifies a part and its position in the object you are uploading.
    Part number you choose need not be a consecutive sequence (for example, it
    can be 1, 5, and 14). If you upload a new part using the same part number as
    a previously uploaded part, the PREVIOUSLY UPLOADED PART is OVERWRITTEN.
    Whenever you upload a part, Amazon S3 returns an ETag header in its
    response. For each part upload, you MUST record the part number and the ETag
    value. You NEED to INCLUDE THESE VALUES in the subsequent request TO
    COMPLETE the multipart UPLOAD.

    Concurrent uploads can easily overwrite each other.  So my take on using
    multipart upload, at least for the IoT use-case, is to NOT have individual
    processes write directly to the multipart upload but instead write to
    a message queue which is then pulled from by ONE process that makes the
    upload.

    Q.  If we upload parts 1 2 3 4 5 and then send the completion request with
    part 3 missing,  do we upload everything except part 3?

    '''

    globals().update(locals())
    return


def upload_parts():
    part_numbers_and_etags = []
    upload_id = 'afzjzih7GxIPvt6Vh2Bya_nDTQ03uplSLorUXFdkP.iaZZuPb.h63YAD9zpzJbV_QDcmQFizNeBBh._FKEfq6QmZPSDOa3mPIcovIxz2ik8mS.Nu3DYPxosal5DLTReM1dWJS.myu9y356aJeXqz6S7IjKrPu2roue71KXmHV.o-'
    bucket_name
    key = 'some/multipart/upload/foo_1515763723.2323093.txt'
    for i in range(1,6):
        part_number = i
        body = '%s' % part_number * 6 * 10**6  # > 5 MB
        body += '\n'
        # because all parts except the last must be >= 5MB.
        params = dict(
            UploadId = upload_id,
            Bucket = bucket_name,
            Key = key,
            PartNumber = part_number,
            Body = body,
            )
        print(time.ctime())
        part_response = s3.upload_part(**params)
        # Sheesh!  Super-Slow!
        assert part_response['ResponseMetadata']['HTTPStatusCode'] == 200
        etag = part_response['ETag']
        part_numbers_and_etags.append((part_number, etag))
        print(time.ctime())
        print(part_numbers_and_etags)
        print('')
    globals().update(locals())
    return


def complete_upload():
    '''
    complete-multipart-upload
    list_parts
    '''

    # list parts
    upload_id = 'afzjzih7GxIPvt6Vh2Bya_nDTQ03uplSLorUXFdkP.iaZZuPb.h63YAD9zpzJbV_QDcmQFizNeBBh._FKEfq6QmZPSDOa3mPIcovIxz2ik8mS.Nu3DYPxosal5DLTReM1dWJS.myu9y356aJeXqz6S7IjKrPu2roue71KXmHV.o-'
    bucket_name = 'cm-test-multipart-upload'
    key = 'some/multipart/upload/foo_1515763723.2323093.txt'
 
    params = dict(
        UploadId = upload_id,
        Bucket = bucket_name,
        Key = key,
        )
    response = s3.list_parts(**params)
    assert response['ResponseMetadata']['HTTPStatusCode'] == 200
    part_list = response['Parts']


    # complete upload
    _ = [dct.pop('Size') for dct in part_list]
    _ = [dct.pop('LastModified') for dct in part_list]
    params['MultipartUpload'] = {
          'Parts': [
              {
                  'ETag': 'string',
                  'PartNumber': 123
              },
          ]
      }
    # pop one item.  See if upload completes.
    part_list.pop(2)
    # Yes, it completes.
    # No problemo.
    # Completes without all parts, no problem.  Ick!
    params['MultipartUpload'] = { 'Parts': part_list }

    response = s3.complete_multipart_upload(**params)
    assert response['ResponseMetadata']['HTTPStatusCode'] == 200

    globals().update(locals())
    return


# ###################################################### #
# ############     s3 file size dilemma     ############ #
# ###################################################### #
# 
# Kinesis firehose can buffer 1MB-128MB and up to 900 sec (15 min).
# Snowflake (built on redshift) loads from s3 and LIKES (not REQUIRES) to have
# min s3 file size of 100MB.
#
# So can firehose do multipart uploads?  NO.  
#    Redshift 
#    ES 
#    S3  ordinary 
#    I think that's it.
#
# So if we have lots of IoT devices coming from everywhere we want to aggregate
# into an S3 file.  But multipart upload will not work because we require unique
# part numbers.  So write to a message queue.  Then a single process pulls from
# the MQ and writes to S3 via multipart upload.
#
# And the aggregated S3 files do not have to be huge.  Well maybe they do but we
# do not know that in advance.  So forcing big s3 files is premature
# optimization.  So aggregate a bit and see how things work.  If not good enough
# then make a change, increase s3 file size or whatever.
#
# So wait a minute.  That means multipart upload as I'm doing here may be
# premature optimization.  Firehose aggregates by time and size so just try it
# out and see how it goes.
#
# And after all, firehose is doing something like multipart uploads when it does
# its buffering.
# And what happens to the S3 files?  Obviously they get used for further
# analysis.  Probably each reader has its own preferences re size?
#



