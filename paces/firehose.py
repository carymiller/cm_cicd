from exploratorium import make_session
from util import remember, timeit
import json
import time
# TODO:  find account name (from account # ex 207841976238)
# TODO:  next Write to some FH stream with fake water meter data.
#        From multi different locations.   Stream it fast and see how fast FH
#        cranks it out.
# As step 1 of that, just stream a bit of water meter data to one fh-stream and
# then ...?  to redshift maybe.  or one or another DB location.  DDB  etc.


region = 'us-east-1'
profile = 'administrator-47lining-test5'
this = make_session(profile_name=profile, region_name=region)
s3 = this.session.client('s3', region)
fh = this.session.client('firehose', region)
iam = this.session.client('iam', region)    # iam.create_role


#@remember
def bucket_contents(bucket='fake-data-cmtest-fh1'):
    '''>>> bucket_contents('fake-data-cmtest-fh1')
    '''
    response = s3.list_objects(Bucket=bucket)
    try:
        return [d['Key'] for d in response['Contents']]
    except KeyError:  # no 'Contents' in response
        return []


def object_contents(
    bucket='fake-data-cmtest-fh1', 
    key= 'fake/2018/01/15/21/cm-test-1516052790-1-2018-01-15-21-46-54-f282d3ac-7e38-4b11-ba01-769783d55401'):
    foo = s3.get_object(Bucket=bucket, Key=key)
    bod = foo['Body']
    return bod.read()


def streams():
    response = fh.list_delivery_streams(Limit=111)
    assert response['ResponseMetadata']['HTTPStatusCode'] == 200
    assert not response['HasMoreDeliveryStreams']
    creams = response['DeliveryStreamNames']
    for name in creams:
        print(name)
        desc = fh.describe_delivery_stream(DeliveryStreamName = name)
        assert desc['ResponseMetadata']['HTTPStatusCode'] == 200
        desc = desc['DeliveryStreamDescription']
#        print(desc['Destinations'])
#        print('')

    globals().update(locals())
#    verbose and print('\n'.join(streams))
#    verbose and print('===========================')


def delivery_streams():
    return fh.list_delivery_streams()['DeliveryStreamNames']

 
def fun_with_fh():
    '''
    list_delivery_streams(
    create_delivery_stream(
    delete_delivery_stream(
    describe_delivery_stream(
    generate_presigned_url(
    get_paginator(
    get_waiter(
    put_record(
    put_record_batch(
    update_destination(
    '''
    verbose = False
    verbose = True
    ts = int(time.time())

    # delete_delivery_stream
    try:
        fu = fh.delete_delivery_stream(DeliveryStreamName = 'cm-test-1515945577.219642')
    except: pass

    # list_delivery_streams
    response = fh.list_delivery_streams()
    assert response['ResponseMetadata']['HTTPStatusCode'] == 200
    treams = response['DeliveryStreamNames']
    verbose and print('\n'.join(treams))
    verbose and print('===========================')

#    globals().update(locals())
#    return

    # create_delivery_stream(

    # 1.  Get arns for role and bucket.
    response = iam.get_role(RoleName = 'cm_test_role')
    role = response['Role']  # identical to role above.
    arn = role['Arn']
    bucket_name = 'fake-data-cmtest-fh1'
    barn = 'arn:aws:s3:::%s' % bucket_name

    # 2.  Make the call.
    name = 'cm-test-%s' % ts
    params = dict(
        DeliveryStreamName=name,
        DeliveryStreamType='DirectPut',
        ExtendedS3DestinationConfiguration = dict(
            RoleARN = role['Arn'],
            BucketARN = barn,
            Prefix='fake/',
            BufferingHints = dict(
                SizeInMBs = 11,
                IntervalInSeconds = 600
                )
            ),
        )
    response = fh.create_delivery_stream(**params)
    assert response['ResponseMetadata']['HTTPStatusCode'] == 200
#    assert response['Location'][1:] == bucket_name
    verbose and print('yoohoo')

    # describe_delivery_stream
    desc = fh.describe_delivery_stream(DeliveryStreamName = name)
    verbose and print(desc['DeliveryStreamDescription'].keys())
#    verbose and print(desc['DeliveryStreamDescription'])
    status = desc['DeliveryStreamDescription']['DeliveryStreamStatus']
    '''Wait for status to become ACTIVE then post to it.  '''

    while status != 'ACTIVE':
        # Takes ~30 sec to become active.
        time.sleep(10**1)
        desc = fh.describe_delivery_stream(DeliveryStreamName = name)
        status = desc['DeliveryStreamDescription']['DeliveryStreamStatus']
        verbose and print(name, status)

    # put_record
#    name = 'cm-test-1515945577.219642'
    for i in range(22):
        msg = 'yoohoo %s\n' % time.time()
        pr = fh.put_record(
            DeliveryStreamName=name,
            Record={'Data': msg })
        record_id = pr['RecordId']
        verbose and print(msg, i)

    globals().update(locals())
    return


# ###################################################### #
# ############     s3 file size dilemma     ############ #
# ###################################################### #
# 
# Kinesis firehose can buffer 1MB-128MB and up to 900 sec (15 min).
# Snowflake (built on redshift) loads from s3 and LIKES (not REQUIRES) to have
# min s3 file size of 100MB.
#
# So can firehose do multipart uploads?  NO.  
#    Redshift 
#    ES 
#    S3  ordinary 
#    I think that's it.
#
# So if we have lots of IoT devices coming from everywhere we want to aggregate
# into an S3 file.  But multipart upload will not work because we require unique
# part numbers.  So write to a message queue.  Then a single process pulls from
# the MQ and writes to S3 via multipart upload.
#
# And the aggregated S3 files do not have to be huge.  Well maybe they do but we
# do not know that in advance.  So forcing big s3 files is premature
# optimization.  So aggregate a bit and see how things work.  If not good enough
# then make a change, increase s3 file size or whatever.
#
# So wait a minute.  That means multipart upload as I'm doing here may be
# premature optimization.  Firehose aggregates by time and size so just try it
# out and see how it goes.
#
# And after all, firehose is doing something like multipart uploads when it does
# its buffering.
# And what happens to the S3 files?  Obviously they get used for further
# analysis.  Probably each reader has its own preferences re size?
#


