'''
'''
try:
    from .util import tag_list_to_dict, partition
    from .exploratorium import make_session
except SystemError:
    from util import tag_list_to_dict, partition
    from exploratorium import make_session
from secrets import profiles


region = 'us-east-1'
profile = profiles['admin_test5']
this = make_session(profile_name=profile, region_name=region)
# tag_resources untag_resources get_tag_keys get_tag_values get_resources
rgt = this.session.client('resourcegroupstaggingapi')
ssm = this.session.client("ssm")
cf = this.session.client("cloudformation")


def resources():
    '''
    Get all ec2 instances in profile/region.
    '''
    paginator = rgt.get_paginator('get_resources')
    page_iterator = paginator.paginate(
#        ResourceTypeFilters = ['sts', 'ec2:instance'],
        ResourceTypeFilters = ['ec2:instance'],
        ResourcesPerPage=5,  
        )
    all_resources = []
    for page in page_iterator:
#        print(page['PaginationToken'], len(page['ResourceTagMappingList']))
        all_resources.extend(page['ResourceTagMappingList'])
    return all_resources 


def cm_resources():
    res = resources()
    for thing in res:
        tags = tag_list_to_dict(thing['Tags'])
        if 'Group' in tags and tags['Group'] == 'cmiller':
            assert 'aws:cloudformation:stack-name' in tags and tags['aws:cloudformation:stack-name'].startswith('cm-stack-')
            stackid = tags['aws:cloudformation:stack-id']
#            cf.delete_stack(StackName=stackid)
            bum = cf.describe_stacks(StackName=stackid)['Stacks'][0]
            assert  bum['StackStatus'] == 'DELETE_COMPLETE'
#            print(tags)
#            print(stackid, '\n')

        globals().update(locals())


def arn_of(thing):
    return thing['ResourceARN']


def name_of(thing):
    tags = tag_list_to_dict(thing['Tags'])
    return tags['Name'] if 'Name' in tags else thing['ResourceARN'].split('/')[1]


