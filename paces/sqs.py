'''
sqs
minimal ec2 and tagging with resourcegroupstaggingapi. 
'''


from exploratorium import make_session
from util import remember, timeit
import json
import time


region = 'us-east-1'
profile = 'administrator-47lining-test5'
this = make_session(profile_name=profile, region_name=region)
sqs = this.session.client("sqs", region)
ec2 = this.session.client("ec2", region)
rgt = this.session.client('resourcegroupstaggingapi', region)

# 
cogi = this.session.client('cognito-identity', region)
cogidp = this.session.client('cognito-idp', region)
cogs = this.session.client('cognito-sync', region)
lup = cogidp.list_user_pools(MaxResults=11)



@timeit
def ec2_():
    #tag_instance(instance_id, status)
    iid = 'i-0ccd79005fc6ddd70'
    sd = ec2.describe_instance_status(InstanceIds=[iid])
    assert sd['InstanceStatuses'][0]['InstanceState']['Name'] == 'running'
    # Very useful info above
#    bar = ec2_client.describe_instances(InstanceIds=[iid])
    globals().update(locals())


@timeit
def tagging_():
    response = rgt.get_resources(ResourceTypeFilters=['ec2:instance'],
            TagFilters=[dict(Key='foo')])
    proof = [list(dct.values()) for dct in response['ResourceTagMappingList'][0]['Tags'] if list(dct.values()) == 'foo bar'.split()]

    # or is there another way?
    # Obviously one checks the return value from the tag_resources call.
    # and when doing that does one look at http response code or
    # FailedResourcesMap?   I think FailedResourcesMap.

    tr = tag_resource(iid, dict(roo='bar'))
    if tr['FailedResourcesMap'] == {}: success=True  # ???
    # or ?
    if tr['ResponseMetadata']['HTTPStatusCode'] == 200:  success=True
    # If the action is successful, the service sends back an HTTP 200 response.
    # So checking for 200 is fine I think.
    globals().update(locals())



#@remember
def sqs_():
    '''Getting feet wet with common operations.
    '''

    # list all queues
    qrls = sqs.list_queues()['QueueUrls']
    print('listed queues')

    # create queue
    qname = 'remote-func-%s' % int(time.time())
    response = sqs.create_queue(QueueName=qname)
    assert response['ResponseMetadata']['HTTPStatusCode'] == 200
    qrl = response['QueueUrl']
    print('created queue')

    # send a message
    foo = sqs.send_message(QueueUrl=qrl, MessageBody='yoohoo')
    assert foo['ResponseMetadata']['HTTPStatusCode'] == 200
    # foo['MessageId'] '18516b01-2993-45a6-9f38-899ebcc74603'
    # foo['MD5OfMessageBody'] '6a75ea5a63f9e5d42a8b8aad98738c96'
    print('sent a message')

    # send a message
    instance_id = 'i-123456'
    def f(): return
    func = f
    command_id = 'cmd-654321'
    n = 0
    msg = json.dumps((instance_id, func.__name__, command_id, n))
    foo = sqs.send_message(QueueUrl=qrl, MessageBody=msg)
    assert foo['ResponseMetadata']['HTTPStatusCode'] == 200
    print('sent a message')

    # tag it
    foo = sqs.tag_queue(QueueUrl=qrl, Tags=dict(purpose='remote command'))
    assert foo['ResponseMetadata']['HTTPStatusCode'] == 200
    print('tagged it')
    foo = sqs.list_queue_tags(QueueUrl=qrl)
    assert foo['Tags'] == {'purpose': 'remote command'}
    print('listed tags')

    # get url
    qrl = sqs.get_queue_url(QueueName=qname)['QueueUrl']

    foo = sqs.untag_queue(QueueUrl=qrl, TagKeys=['purpose'])
    assert foo['ResponseMetadata']['HTTPStatusCode'] == 200
    print('got url')

    # attributes
#    qrl = 'https://queue.amazonaws.com/466244873177/lambda-errors'
#    qrl = 'https://queue.amazonaws.com/466244873177/lambda-results'
    bar = sqs.get_queue_attributes(QueueUrl=qrl, AttributeNames=['All'])['Attributes']
    print('got attributes')

    # batch send/receive
    messages = 'yoo hoo moo goo bloo tattoo foo'.split()
    Entries = [dict(MessageBody=msg, Id=str(i)) for (i, msg) in enumerate(messages)]
    bark = sqs.send_message_batch(QueueUrl=qrl,  Entries=Entries)
    assert bark['ResponseMetadata']['HTTPStatusCode'] == 200
    assert len(bark['Successful']) == len(Entries)
    print('sent batch')

    # get a message
    foom = sqs.receive_message(QueueUrl=qrl, AttributeNames=['All'])
    assert foom['ResponseMetadata']['HTTPStatusCode'] == 200
#    assert foom['Messages'][0]['Body'].endswith('oo')
    print('got a message')

    # delete queue
    dr = sqs.delete_queue(QueueUrl=qrl)
    assert dr['ResponseMetadata']['HTTPStatusCode'] == 200
#    assert dr is None

    globals().update(locals())
    return

