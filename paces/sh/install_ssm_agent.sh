# install ssm agent.  Should be installed on Amazon Linux AMI's after 2017.09
cd /tmp
sudo yum install -y https://s3.amazonaws.com/ec2-downloads-windows/SSMAgent/latest/linux_amd64/amazon-ssm-agent.rpm
sudo start amazon-ssm-agent
sudo ps aux | grep agent

