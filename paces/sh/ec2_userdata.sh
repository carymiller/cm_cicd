#!/bin/bash
# Runs as root on instance creation.
# Most of the file is book keeping and useful information on the instance.

# make a place to store notes
mkdir /tmp/userdata

yum -y update  # standard advice
yum -y update kernel  # security update

# setup aws cli region
yum -y install jq
export region=$(curl --silent http://169.254.169.254/latest/dynamic/instance-identity/document | jq -r .region)
echo $region > /tmp/userdata/region
export az=$(curl --silent http://169.254.169.254/latest/meta-data/placement/availability-zone)
echo $az > /tmp/userdata/az

echo alias aws=\"aws --region $region\" >> /home/ec2-user/.bashrc
# and why does not aws just do this automatically? duh!


# Record the name used to create resources.
mkdir /tmp/userdata/_name_  # _name_ gets substituted by shell script.


# All done #
# Q.  How to get other files onto the ec2 instance?
# A.  Copy from s3 of course.
date > /tmp/userdata/done_at_$(date +%s)
# I think this is enough for the userdata script.  If more needs to be done on
# the ec2 then it should be in the form of other files downloaded from s3.
# In fact, that could start with the Python3 installation.


# #################### Specifics for the instance go here #################### #
# mount enron volume  
# or water meter simulation 
# or ...



