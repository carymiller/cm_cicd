# Send the water-meter command to $instance_id.
instance_id=$1

aws ssm send-command --document-name "AWS-RunShellScript" \
    --comment "simulated water meter" \
    --instance-ids $instance_id \
    --output text \
    --parameters commands="python3 /home/ec2-user/fakery/simulate_water_meter/push_fake_data.py && date > /tmp/userdata/simulation_ran"


# aws ssm list-command-invocations --command-id b485f9c8-0398-465a-bafb-2575fc4a6fc0 --details


#    --parameters commands="cd /home/ec2-user/fakery; python3 simulate_water_meter/push_fake_data.py && date > /tmp/userdata/simulation_ran"

aws ssm send-command --document-name "AWS-RunShellScript" \
    --comment "security update" \
    --instance-ids $instance_id \
    --output text \
    --parameters commands="yum -y update kernel"


