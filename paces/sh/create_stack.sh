# Expects to run from the template directory
# Requires $name parameter
#
export tdir=$(echo file://$(pwd | sed 's#/#//#g'))
export name=cmtest-fake-1
export name=$1
shift
__files_to_install__=$@
echo __files_to_install__ $__files_to_install__
export az=$(aws configure get region)a

# create the stack and make a note of the stackid
export stackid=$(aws cloudformation create-stack \
    --stack-name $name \
    --on-failure DO_NOTHING --template-body $tdir//ec2_with_role.yaml \
    --parameters  \
        ParameterKey=tagName,ParameterValue=$name \
        ParameterKey=UserData,ParameterValue=$(
            cat ec2_userdata_proto.txt $__files_to_install__ | 
                sed "s/_name_/$name/" | base64 -w0)  \
        ParameterKey=az,ParameterValue=$az | jq .StackId | sed 's/"//g')

echo $stackid
echo $stackid > /tmp/current/stackid
exit 0
# Wait a minute for instance_id to be available.

# TODO:  make a note ON THE INSTANCE of what was installed.
# Could be done with a second/tail userdata file?
#
