

# Generate fake IoT data (requires python 3)
# Get the code from an S3 bucket.  No credentials required.
cd /home/ec2-user
mkdir fakery
cd fakery
aws s3 cp s3://cm-fake-data-code/ec2_simulate.tar .
tar xvf ec2_simulate.tar 
# Name, ec2_simulate.tar, must be synchronized with update_simulation_archive.sh
pip3 install boto3    # globally (no virtualenv)


# Below is really about running. and should go elsewhere.  Like if we want to
# run on a schedule.
# 
# simulated water meter
python3 simulate_water_meter/push_fake_data.py && date > /tmp/userdata/simulation_ran

