stackid=$1
foo=$(aws cloudformation list-stack-resources --stack-name $stackid)
#echo $foo | jq .

instance_id=$(echo $foo | jq .StackResourceSummaries[].PhysicalResourceId | 
    grep '"i-' | sed 's/\"//g')

instance_ip=$(aws ec2 describe-instances  --instance-ids $instance_id | 
    jq .Reservations[].Instances[].PublicIpAddress | sed "s/\"//g")

echo stackid $stackid 
echo instance_id $instance_id
echo instance_ip $instance_ip
echo $stackid > /tmp/current/stackid
echo $instance_id > /tmp/current/instance_id
echo $instance_ip > /tmp/current/instance_ip

# go there
ssh -i /home/cary/.ssh/cm_test5.pem ec2-user@$instance_ip

echo stackid $stackid 
echo instance_id $instance_id
echo instance_ip $instance_ip


