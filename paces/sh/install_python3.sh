# install python 3
# would be great if this worked.
# sudo yum install python36 python36-virtualenv python36-pip
# but it does not.
# so instead we do this

# ################################ #
# cd /tmp/
# aws s3 cp s3://cm-fake-data-code/install_python3.sh .
# latest=3.6.4
# bash install_python3.sh $latest
# Python 3 is now installed.   Leave evidence of it.
# python3 -m os && date > /tmp/userdata/python3_installed
# ################################ #

sudo yum -y groupinstall 'Development Tools' # otherwise pip won't install.
sudo yum -y install openssl-devel

latest=3.6.4
#latest=$1

cd /tmp/
wget https://www.python.org/ftp/python/$latest/Python-$latest.tgz
tar zxvf Python-$latest.tgz
cd Python-$latest
./configure --prefix=/opt/python3
make
make install
cd ..
#sudo rm -rf Python-$latest*

ln -s /opt/python3/bin/python3 /usr/bin/python3
ln -s /opt/python3/bin/pip3 /usr/bin/pip3
# Python 3 is now installed

date > /tmp/installed_python_$latest

