import time
import os
from exploratorium import make_session
from util_aws import return_code_check, prams
from secrets import region, profile, tdir

this = make_session(profile_name=profile, region_name=region)
cf = this.session.client('cloudformation')


def describe(stackname):
    return cf.describe_stacks(StackName=stackname)['Stacks'][0]


def validate(tname):
    TemplateBody = open(os.path.join(tdir, tname)).read()
    del tname
    foo = cf.validate_template(**locals())
    return_code_check(foo)
    return foo


def setup_cognito():
    '''
    >>> stackid = setup_cognito()
    '''
    name = 'shoo%s' % int(time.time())
    name = 'cognito%s' % int(time.time())
    tname = 'cognito.yaml'
    tups = [('AuthName', name), ]
    return create(name, tname, tups)


def create(name, tname, tups):
    kwargs = dict(
        StackName=name,
        OnFailure='DO_NOTHING',
        TemplateBody=open(os.path.join(tdir, tname)).read(),
        Capabilities=['CAPABILITY_IAM'],
        )
    if tups:
        kwargs['Parameters'] = prams(tups)
    foo = cf.create_stack(**kwargs)
    return_code_check(foo)
    assert list(foo.keys()) == ['StackId']
    return foo['StackId']


def upid_for_stackid(stackid):
    for dct in resources(stackid):
        if dct['ResourceType'] == 'AWS::Cognito::UserPool':
            return dct['PhysicalResourceId']


def delete(stackname):
    response = cf.delete_stack(StackName=stackname)
    return_code_check(response)
    return response 


def resources(stackid):
    bark = cf.list_stack_resources(StackName=stackid)
    return_code_check(bark)
    bf = bark['StackResourceSummaries']
    bu = cf.describe_stack_resources(StackName=stackid)['StackResources']
    return bu


def find_completed_stack(prefix='shoo'):
    '''
    >>> find_completed_stack()
    ('shoo1516994595', 'arn:aws:cloudformation:us-east-1:551008831986:stack/shoo1516994595/614a7bf0-02ce-11e8-9dfb-503aca2616c5')
    '''
    paginator = cf.get_paginator('list_stacks')
    page_iterator = paginator.paginate()
    for page in page_iterator:
        return_code_check(page)
        for summ in page['StackSummaries']:
            sn = summ['StackName']
            sid = summ['StackId']
            ss = summ['StackStatus']
#            if ss == 'CREATE_COMPLETE': print(sn)
#            if sn.startswith(prefix): print(sn, ss)
#            if sn.startswith(prefix) and ss != 'DELETE_COMPLETE': print(sn, ss)
            if sn.startswith(prefix) and ss in 'CREATE_COMPLETE UPDATE_ROLLBACK_COMPLETE':
                return (sn, sid)


def list_stacks():

    paginator = cf.get_paginator('list_stacks')
    page_iterator = paginator.paginate()
 
    i = 0
    j = 0
    print('page', i, time.time(), j)
    for page in page_iterator:
        i += 1
        return_code_check(page)
        for summ in page['StackSummaries']:
            j += 1
            sn = summ['StackName']
            sid = summ['StackId']
            ss = summ['StackStatus']
            if sn.startswith('shoo') or sn.startswith('smoo'):
                print('   ', sn, ss, sid)
                if ss == 'DELETE_COMPLETE':
                    continue
                if ss == 'CREATE_COMPLETE':
                    print('   ', sn, ss)
                    sid = summ['StackId']
                    break
                if ss == 'CREATE_FAILED':
                    delete(sn)
#                ss = describe(sn)
                print('   ', sn, ss)
        break
        print('page', i, time.time(), j)
    globals().update(locals())
    return j


