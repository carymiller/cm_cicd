
# Deploy the lambda from boto3 directly.
# But the role can still be created from the template.


'''
LambdaFunction:
    Type: "AWS::Lambda::Function"
    Properties: 
      Code: 
        S3Bucket: "cm-delete-me"
        S3Key: "lamb0.zip"
      Description: Experiment N=0
#      Environment: 
#         - Env: dev
#         - shmenv: foo bar x
#           Value of property Environment must be an object
#           Value of property Environment must be an object
#           Value of property Environment must be an object
      FunctionName: cm-delete-me
      Handler: go_lambda
  #    Handler: lamb0.go_lambda
      MemorySize: 128
      ReservedConcurrentExecutions: 11
      Role:
        Fn::GetAtt: 
          - "LambdaExecutionRole"
          - "Arn"
      Runtime: python3.6
      Timeout: 33
 
'''
