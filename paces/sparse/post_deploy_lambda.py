'''
post-cognito lambda.
'''


from exploratorium import make_session
from util_aws import return_code_check
import time
from secrets import region, profile, tdir

this = make_session(profile_name=profile, region_name=region)
cf = this.session.client('cloudformation', region)
lamb = this.session.client('lambda', region)


def go_lambda(event, context):
    '''
    '''
    return dict(keys =event.keys())

todo = '''
    - this code into lambda.
        - requires boto3
'''

doc = '''
- CF template runs via CodePipeline. 
- Then post-deploy python code runs via lambda. 


https://docs.aws.amazon.com/lambda/latest/dg/current-supported-versions.html
boto3 is builtin to lambda python runtime.
python 3.6

The lambda is declared in the cognito template, along with the other resources.
? or is it ?
If it is declared in the cognito template, the lambda can run DURING creation of
resources.  
https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/template-custom-resources-lambda.html?shortFooter=true
But I currently want lambda AFTER stack completion.
So I want an EVENT to be triggered by stack completion.

'''


questions = '''
- Lambda runs in response to events.  Can stack completion trigger an event?

'''


