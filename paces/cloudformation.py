'''
Cloudformation basics.

boto3 vs cloudformation.
Cloudformation is best for creating stacks because all resources are grouped
together in the stack.  Cloudformation does updates but it has a limited
reportoire and rigid requirements.  Boto3 is better for updates that fall
outside the CF reportoire.  Boto3 is good for running CF, just as it is for
other AWS subunits.

'''

from exploratorium import make_session
from util import remember, timeit, omit
import json
import time
import os


region = 'us-east-1'
profile = 'administrator-47lining-test5'
this = make_session(profile_name=profile, region_name=region)
ec2 = this.session.client("ec2", region)
cf = this.session.client('cloudformation', region)


StackStatus = [
'CREATE_IN_PROGRESS','CREATE_FAILED','CREATE_COMPLETE','ROLLBACK_IN_PROGRESS','ROLLBACK_FAILED','ROLLBACK_COMPLETE','DELETE_IN_PROGRESS','DELETE_FAILED','DELETE_COMPLETE','UPDATE_IN_PROGRESS','UPDATE_COMPLETE_CLEANUP_IN_PROGRESS','UPDATE_COMPLETE','UPDATE_ROLLBACK_IN_PROGRESS','UPDATE_ROLLBACK_FAILED','UPDATE_ROLLBACK_COMPLETE_CLEANUP_IN_PROGRESS','UPDATE_ROLLBACK_COMPLETE','REVIEW_IN_PROGRESS',
]
# TODO:  aws constants.  Get any such at the drop of a hat.



def slow():
    t0 = time.time()
    foo = cf.list_stacks()
    assert foo['ResponseMetadata']['HTTPStatusCode'] == 200
    nt = foo['NextToken']
    i = 0
    j = 0
    while 'NextToken' in foo:
        i += 1
        foo = cf.list_stacks(NextToken = foo['NextToken'])
        ss = foo['StackSummaries']
        for thing in ss:
            j += 1
#            name = thing['']
            status = thing['StackStatus']
            name = thing['StackName']
            if status != 'DELETE_COMPLETE':
                print(name, status, j)
    t1 = time.time() - t0
    print(t1, '\n')                    # 61 sec


def cf_():
    '''
    create_stack
    delete_stack
    update_stack
    list_stacks
    describe_stacks
    validate_template
    get_template  # for updates
    create_change_set # for updates
    '''
    # omitting DELETE_COMPLETE
    t0 = time.time()
    om = omit(['DELETE_COMPLETE'], StackStatus)
    bar = cf.list_stacks(StackStatusFilter = om)
    assert 'NextToken' not in bar
    for thing in bar['StackSummaries']:
        name = thing['StackName']
        status = thing['StackStatus']
        print(name, status)
    t1 = time.time() - t0
    print(t1, 'n=%s\n' % len(bar['StackSummaries']))     # 1 sec
    # So StackStatusFilter is much faster, at least in this case.

    # only non-deleted stacks.
    t0 = time.time()
    ds = cf.describe_stacks()
    assert ds['ResponseMetadata']['HTTPStatusCode'] == 200
    for thing in ds['Stacks']:
        pass
    t1 = time.time() - t0
    print(t1, 'n=%s\n' % len(ds['Stacks']))     # 1 sec

    assert len(ds['Stacks']) == len(bar['StackSummaries'])
    return


    # get_template(
    qsr = cf.get_template(StackName='qs')
    qst = qsr['TemplateBody']
    dlr = cf.get_template(StackName='dlf')
    dlt = dlr['TemplateBody']
    cmr = cf.get_template(StackName='cmtest-fake1')
    cmt = cmr['TemplateBody']

    # validate_template(
    cmvr = cf.validate_template(TemplateBody=cmt)
    qsvr = cf.validate_template(TemplateBody=json.dumps(qst))
    dlvr = cf.validate_template(TemplateBody=json.dumps(dlt))


# TODO:  change sets, updates, etc.
# A couple of easy examples of making changes like tags.
# And of course before that, a couple of simple create_stack examples.
# Naturally using the fake data ec2 thingies.
# And then maybe contrast with making changes via plain vanilla boto3.


#@timeit
def ec2_():
    '''
    '''
    #tag_instance(instance_id, status)
    iid = 'i-0ccd79005fc6ddd70'
    sd = ec2.describe_instance_status(InstanceIds=[iid])
    assert sd['InstanceStatuses'][0]['InstanceState']['Name'] == 'running'
    # Very useful info above

    bar = ec2.describe_instance_status()
    for thing in bar['InstanceStatuses']:
        iid = thing['InstanceId']
        state = thing['InstanceState']['Name'] 
        dct = thing['InstanceStatus']['Details'][0]
        r, s = (dct['Name'], dct['Status'])
        print(iid, state, r, s)

    di = ec2.describe_instances(InstanceIds=[iid])
    dr = di['Reservations'][0]
    ins = dr['Instances'][0]

    # Hey.  This is cf so why doing all this ec2 stuff?
    # and where are the tags/names?
    # Don't answer that.  Move it to the ec2 module.

    return


sdir = '/home/cary/47L/code/cicd/paces/sh'
tdir = '/home/cary/47L/code/cicd/paces/templates'

dlv2_questions = '''
    1. How are templates written?  Directly?  or generated somehow?
       - Like from dictionaries?
    2. How is userdata put into templates?
       - ie,  are the raw scripts somewhere?
       - point being, keeping scripts in the form of quoted list items in a CF
         template sucks.

    and notes
    * pytest is groovy.  Worth delving into.
    * 
    * 
'''


# Construct userdata by concatenating scripts from the sh/ directory.
# sh/ec2_userdata.sh
# sh/install_python3.sh
# That is,  all userdata scripts start with sh/ec2_userdata and then whatever
# other script.


def fu():
    '''Create a stack.

    '''
    import base64
    ts = int(time.time())
    name = 'cm-stack-%s' % ts
    tb = os.path.join(tdir, 'ec2_with_role.yaml')
    with open(tb) as fh:
        tb = fh.read()

    # ########## construct userdata script ########## #
    extra_ud = ['install_python3.sh', 'setup_enron.sh']

    # basic userdata script
    ud1 = os.path.join(sdir, 'ec2_userdata.sh')
    with open(ud1) as fh: 
        ud1 = fh.read()
    udt = ud1.replace('_name_', name)

    # append script(s) to userdata.
    for script in extra_ud:
        ud2 = os.path.join(sdir, script)
        with open(ud2) as fh: 
            ud2 = fh.read()
        udt += ud2

    udb = base64.b64encode(udt.encode('utf-8'))
    ud = str(udb)[2:-1]

    # ########## template parameters ########## #

    az = 'us-east-1a'
    tag = 'yoohoo-%s' % int(time.time())
    tups = [('tagName', tag), ('az', az), ('UserData', ud), ]
    f = lambda key, value: dict(ParameterKey=key, ParameterValue=value)
    parameters = [f(key, value) for (key, value) in tups]

    # ########## validate template ########## #
    boof = cf.validate_template(TemplateBody=tb)

    # ########## go ########## #
    foob = cf.create_stack(StackName=name, OnFailure='DO_NOTHING',
            TemplateBody=tb,  Parameters=parameters)
    assert foob['ResponseMetadata']['HTTPStatusCode'] == 200
    return foob['StackId']


@remember
def follow(stackid):
    bark = cf.list_stack_resources(StackName=stackid)
    srs = bark['StackResourceSummaries']  # ==[] immediately after create_stack.

    tr = lambda thing: thing['PhysicalResourceId'] 
    good_one = lambda thing: tr(thing).startswith('i-')
    instance_id = [tr(thing) for thing in srs if good_one(thing)][0]

    instance_desc = ec2.describe_instances(InstanceIds=[instance_id])
    return instance_desc['Reservations'][0]['Instances'][0]['PublicIpAddress']
    globals().update(locals())



def x():
    # from security update #
    org = this.session.client("organizations", region)
    ogr = org.describe_organization()
    assert ogr['ResponseMetadata']['HTTPStatusCode'] == 200
    ogr.pop('ResponseMetadata')
    sts = this.session.client("sts", region)
    gci = sts.get_caller_identity()        #['Account']


# stackid = fu()
# follow(stackid)

#  vim convert stackName to StackName and the other way too.
#      convert snake_case to CamelCase

#  CreateStack StackName OnFailureXy TemplateBodyX 
#  createStack stack-name on-failure-xy template-body-x 
#  createStack stack-name on-failure-xy template-body-x 
#  create-stack stack-name on-failure-xy template-body-x 
#  createStack stackName onFailure-xy template-body-x 
#  create-stack stack-name on-failure-xy template-body-x 
#  create-stack stack-name on-failure-xy template-body-x 
#  createStack stackName onFailureXy templateBodyX 
#   createStack stackName onFailureXy template-body-x 
#  CreateStack StackName onFailureXy templateBodyX 


