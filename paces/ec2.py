'''
minimal ec2 and tagging with resourcegroupstaggingapi. 
'''

from exploratorium import make_session
from util import remember, timeit
import json
import time


region = 'us-east-1'
profile = 'administrator-47lining-test5'
this = make_session(profile_name=profile, region_name=region)
ec2 = this.session.client("ec2", region)
rgt = this.session.client('resourcegroupstaggingapi', region)


#@timeit
def ec2_():
    '''
    describe_instances(
    describe_instance_status(InstanceIds=[iid])
    '''
    #tag_instance(instance_id, status)
    iid = 'i-0ccd79005fc6ddd70'
    sd = ec2.describe_instance_status(InstanceIds=[iid])
    assert sd['InstanceStatuses'][0]['InstanceState']['Name'] == 'running'
    # Very useful info above

    bar = ec2.describe_instance_status()
    for thing in bar['InstanceStatuses']:
        iid = thing['InstanceId']
        state = thing['InstanceState']['Name'] 
        dct = thing['InstanceStatus']['Details'][0]
        r, s = (dct['Name'], dct['Status'])
        print(iid, state, r, s)

    di = ec2.describe_instances(InstanceIds=[iid])
    dr = di['Reservations'][0]
    ins = dr['Instances'][0]

    globals().update(locals())
    return


#@timeit
def tagging_():
    response = rgt.get_resources(ResourceTypeFilters=['ec2:instance'],
            TagFilters=[dict(Key='foo')])
    proof = [list(dct.values()) for dct in response['ResourceTagMappingList'][0]['Tags'] if list(dct.values()) == 'foo bar'.split()]

    # or is there another way?
    # Obviously one checks the return value from the tag_resources call.
    # and when doing that does one look at http response code or
    # FailedResourcesMap?   I think FailedResourcesMap.

    tr = tag_resource(iid, dict(roo='bar'))
    if tr['FailedResourcesMap'] == {}: success=True  # ???
    # or ?
    if tr['ResponseMetadata']['HTTPStatusCode'] == 200:  success=True
    # If the action is successful, the service sends back an HTTP 200 response.
    # So checking for 200 is fine I think.
    globals().update(locals())


