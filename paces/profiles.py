import os
import inspect

__file__ = inspect.getframeinfo(inspect.currentframe()).filename
(this_dir, this_fname) = os.path.split(__file__)
home = os.path.expanduser('~')


def profiles():
    # List of all profiles #
    ppath = os.path.join(home, '.aws/credentials')
    with open(ppath) as fh:
        lines = fh.readlines()
    transform = lambda line: line.strip().replace('[', '').replace(']', '')
    return [transform(line) for line in lines if line.startswith('[')]


def classify(profile):
    for name in 'hhc edf anki prologis shell 47lining'.split():
        if name in profile:
            return name


def go():
    for profile in profiles:
        print(profile, classify(profile))



