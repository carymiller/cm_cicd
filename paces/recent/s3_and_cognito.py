'''
s3
cognito
cf
'''

from util_aws import return_code_check
import os
import time
from secrets import region, profile, bucket_name, tdir
from post_cognito_deploy import describe, prams, resources, delete
from post_cognito_deploy import this


s3 = this.session.client("s3", region)
cogidp = this.session.client('cognito-idp', region)
cf = this.session.client('cloudformation', region)
lamb = this.session.client('lambda', region)
iam = this.session.client('iam', region)


def go_s3():
    '''s3
    '''
    foo = s3.list_buckets()
    return_code_check(foo)
    buckets = foo['Buckets']
    names = [dct['Name'] for dct in buckets]
    for name in names:
        continue
        print(name)
        bar = s3.list_objects(Bucket=name)
        return_code_check(bar)
        try:
            objects = bar['Contents']
        except KeyError:
            continue
        keys = [dct['Key'] for dct in objects]
        for key in keys:
            print('  ', key)

    globals().update(locals())
    name = bucket_name
    name = 'cf-pipeline-test'
    name = 'cm-delete-me'
    bar = s3.list_objects(Bucket=name)
    return_code_check(bar)
    objects = bar['Contents']
    keys = [dct['Key'] for dct in objects]
    print(name)
    keys = [dct['Key'] for dct in objects]
    for key in keys:
        print('  ', key)
    return


def validate(tname):
    TemplateBody = open(os.path.join(tdir, tname)).read()
    del tname
    foo = cf.validate_template(**locals())
    return_code_check(foo)
    return foo


def setup_cognito():
    name = 'shoo%s' % int(time.time())
    name = 'cognito%s' % int(time.time())
    tname = 'cognito.yaml'
    tups = [('AuthName', name), ]
    return create(name, tname, tups)


def setup_lambda():
    '''The actual lambda has been removed from lambda.yaml but it still contains
    the Lambda execution role.  So the template is good for that.  The actual
    lambda should be deployed from here.
    So it will then be outside of any stack.
    '''
    name = 'lambda_post_cognito%s' % int(time.time())
    tname = 'lambda.yaml'
    tups = []
    stackid = create(name, tname, tups)
# Could not unzip uploaded file. Please check your file, then try to upload again.
# Above is known bug in CFT+cli/boto3.
# Workaround is to deploy lambda from here.
# Or is it possible to define just the actual lambda code from here?  I don't
# think so.
    blah = finish_lambda_setup(stackid)


def finish_lambda_setup(stackid):
    '''
    stackid = 'arn:aws:cloudformation:us-east-1:551008831986:stack/smoo1517320989/5c579fb0-05c6-11e8-a58d-500c20fefad2'
    '''
    desc = describe(stackid)
    while desc['StackStatus'] != 'CREATE_COMPLETE':
        time.sleep(10)
        desc = describe(stackid)
    # get the arn of the lambda execution role.
    for resource in resources(stackid):
        if resource['ResourceType'] == 'AWS::IAM::Role':
#            assert resource['LogicalResourceId'] == 'LambdaExecutionRole'
            assert resource['ResourceStatus'] == 'CREATE_COMPLETE'
            role_id = resource['PhysicalResourceId']

    role = iam.get_role(RoleName=role_id)['Role']
    role_arn = role['Arn']
#    globals().update(locals())

    fun = lamb.create_function(
        Code = dict(S3Bucket = "cm-delete-me", S3Key = "lamb0.zip"),
        Description = 'Experiment N=0',
        FunctionName = 'cm-delete-me',
        Handler = 'go_lambda',
        Runtime = 'python3.6',
        Timeout = 33,
        MemorySize = 128,
        Role = role_arn,
    )
    # same error as when using template.
    return_code_check(fun)
    return fun
##  #    Handler: lamb0.go_lambda
###      Environment: 
###         - Env: dev
###         - shmenv: foo bar x
###           Value of property Environment must be an object
###           Value of property Environment must be an object
###           Value of property Environment must be an object
##      ReservedConcurrentExecutions: 11



def create(name, tname, tups):
    kwargs = dict(
        StackName=name,
        OnFailure='DO_NOTHING',
        TemplateBody=open(os.path.join(tdir, tname)).read(),
        Capabilities=['CAPABILITY_IAM'],
        )
    if tups:
        kwargs['Parameters'] = prams(tups),
    foo = cf.create_stack(**kwargs)
    return_code_check(foo)
    assert list(foo.keys()) == ['StackId']
    return foo['StackId']

import post_cognito_deploy

# stackid = 'arn:aws:cloudformation:us-east-1:551008831986:stack/smoo1517254299/11400120-052b-11e8-a58d-500c20fefad2'
