from exploratorium import make_session, remember, timeit
import json
import time
# TODO:  find account name (from account # ex 207841976238)


region = 'us-east-1'
profile = 'administrator-47lining-test5'
this = make_session(profile_name=profile, region_name=region)
iam = this.session.client('iam', region)


def go():
    '''
    list_roles
    create_role
    get_role
    delete_role
    '''
    verbose = False
    verbose = True

    # list_roles
    response = iam.list_roles()
    assert not response['IsTruncated']
    for role in response['Roles']: 
        rname = role['RoleName']
        # ignore boring ones.
        prefixes = '''dlf AWS aws- Amazon EMR q Tab work mick
            pio Code Blue lambda s34 '''.split()
        if any(rname.startswith(prefix) for prefix in prefixes): continue
        rp = iam.list_role_policies(RoleName=rname)
        assert rp['ResponseMetadata']['HTTPStatusCode'] == 200
        pnames = rp['PolicyNames']
#        verbose and print(rname, role['AssumeRolePolicyDocument']['Statement'], '\n')
        verbose and print(rname, pnames)

        globals().update(locals())
#    return

    # create_role
    ts = int(time.time())
    role_name = 'cm_test_role_%s' % ts
    role_name = 'cm_role_s3_full_access'
    params = dict(
        RoleName = role_name,
        AssumeRolePolicyDocument = json.dumps(assume_role_policy_document),
        Path = '/opt/ional/',
        Description = 'also optional',
        )
    response = iam.create_role(**params)
    role = response['Role']

    # Policy required for useful work.
    # Attach a policy to allow read/write to s3.
    # arn:aws:iam::aws:policy/AmazonS3FullAccess 
    params = dict(
        RoleName = role_name,
        PolicyArn='arn:aws:iam::aws:policy/AmazonS3FullAccess'
        )
    fu = iam.attach_role_policy(**params)
    assert fu['ResponseMetadata']['HTTPStatusCode'] == 200
    # Policy attached to role.

    params = dict(RoleName = role_name)

    # get_role
    response = iam.get_role(**params)
    role = response['Role']  # identical to role above.
    arn = role['Arn']

    globals().update(locals())
    return

    # delete_role
    de = iam.delete_role(**params)
    assert de['ResponseMetadata']['HTTPStatusCode'] == 200




#  fhDeliveryRole:
#    Type: "AWS::IAM::Role"
#    Properties: 
#      AssumeRolePolicyDocument:    # required
#        # JSON object
#        Version: "2012-10-17"
#        Statement: 
#          - 
#            Effect: "Allow"
#            Principal: 
#              Service: 
#                - "firehose.amazonaws.com"
#            Action: 
#              - "sts:AssumeRole"
#      Path: "/"
#      RoleName: firehose_delivery_role
# other optional args
#	  ManagedPolicyArns: - String
#	  Policies: - Policies
#

assume_role_policy_document = {
    "Version": "2012-10-17", 
    "Statement": [ 
        { 
            "Effect": "Allow", 
            "Principal": { "Service": "firehose.amazonaws.com" }, 
            "Action": "sts:AssumeRole" } ] }
#            "Principal": { "Service": "ec2.amazonaws.com" }, 

def x():
    # http://boto3.readthedocs.io/en/latest/guide/iam-example-policies.html
    policy_document = {
        "Version": "2012-10-17",
        "Statement": [
            {
                "Effect": "Allow",
                "Action": "logs:CreateLogGroup",
                "Resource": "RESOURCE_ARN"
            },
            {
                "Effect": "Allow",
                "Action": [
                    "dynamodb:DeleteItem",
                    "dynamodb:GetItem",
                    "dynamodb:PutItem",
                    "dynamodb:Scan",
                    "dynamodb:UpdateItem"
                ],
                "Resource": "RESOURCE_ARN"
            }
        ]
    }
    response = iam.create_policy(
      PolicyName='myDynamoDBPolicy',
      PolicyDocument=json.dumps(policy_document)
    )
    print(response)



