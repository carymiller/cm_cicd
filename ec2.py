from pprint import pprint

from ut.ds.lfunc import partition
from ut.alter.func import remember
from ut.alter.aws import return_code_check, pop_top
from ut.alter.object import decorate_methods
from ut.ds.lfunc import flatten
from ut.auth.mfa import make_session
from ut.ds.dlist import tag_list_to_dict
from ut.ds.dlist import extract_common_values

from secrets import region, profile


this = make_session(profile_name=profile, region_name=region)
iam = this.session.client('iam')
ec2 = this.session.client('ec2')
autoscaling = this.session.client('autoscaling')
for ob in (ec2, autoscaling, iam):
    composed = lambda func: pop_top(return_code_check(func))
    decorate_methods(ob, composed, include=None, exclude=None,)
ec2r = this.session.resource('ec2')


# ################################### IAM ################################### #


def list_instance_profiles():
    return iam.list_instance_profiles()['InstanceProfiles']


def list_roles():
    return iam.list_roles()['Roles']


# ################################## NACLs ################################## #
class NoNaclEntry(Exception): pass


def nacl_entries(naclid):
    '''Return nacl entries partitioned by 'Egress'.
    >>> entries = nacl_entries('acl-4b043330')
    '''
    network_acl = ec2r.NetworkAcl(naclid)
    entries = network_acl.entries
    return partition(entries, lambda dct: dct['Egress'])


def nacl_rule_number_for_cidr(cidr, naclid, Egress):
    for (num, block) in nacl_rule_numbers(naclid, Egress):
        if cidr == block:
            return num
    msg = 'no Egress==%s rule for CIDR %s' % (Egress, cidr)
    raise NoNaclEntry(msg)


def nacl_rule_numbers(naclid, Egress=False):
    entries = nacl_entries(naclid)[Egress]
    return [(d['RuleNumber'], d['CidrBlock']) for d in entries]
 
 
def max_nacl_rule_number(naclid, Egress=False):
    return max([num for (num, _) in sorted(nacl_rule_numbers(naclid, False))[:-1]])


def remove_nacl_entry(naclid, Egress, RuleNumber, DryRun=False):
    '''
    >>> response = remove_nacl_entry('acl-4b043330')
    '''
    network_acl = ec2r.NetworkAcl(naclid)
    response = network_acl.delete_entry(
        DryRun=DryRun,
        Egress=Egress,
        RuleNumber=RuleNumber
    )
    return response


def add_nacl_entry(naclid, params):
    '''
    >>> params = dict( CidrBlock='1.2.3.4/32', RuleNumber=125)
    >>> add_nacl_entry(naclid, params)
    '''
    ps = dict(
        DryRun=False,
        Egress=False,
        PortRange={'From': 22, 'To': 22},
        Protocol='6',
        RuleAction='allow',
        )
    ps.update(params)
    network_acl = ec2r.NetworkAcl(naclid)
    return network_acl.create_entry(**ps)


# ############################# Security Groups ############################# #


@remember
def bastion_security_ids(instanceid):
    sgs = instance_security_groups(instanceid)
    iss = instance_subnet(instanceid)
#    nacl_dict = ec2.describe_network_acls(Filters = [{'Name': 'association.subnet-id',  'Values': [iss]}])['NetworkAcls'][0]
    nacl_dict = ec2.describe_network_acls(Filters = [{'Name': 'association.subnet-id',  'Values': [iss]}])[0]
    naclid = nacl_dict['NetworkAclId']
    return (sgs, naclid)


#def bastion_grant_access(instanceid, ipaddress=my_ip()):
#    ([sgid], naclid) = bastion_security_ids(instanceid)
#    cidr = '%s/32' % ipaddress
#    pprint(locals())
#
#    # add to Security Group
#    r = add_cidr_to_sg(sgid, CidrIp=cidr)
#
#    # add to NACL
#    # TODO:  only if not already done.   DONE
#    try:
#        RuleNumber = nacl_rule_number_for_cidr(cidr, naclid, Egress=False)
#        return   # cidr already has access.
#    except NoNaclEntry:
#        rn = max_nacl_rule_number(naclid, Egress=False) + 10
#        params = dict(CidrBlock=cidr, RuleNumber=rn)
#        # TODO:  automatic RuleNumber   DONE
#        add_nacl_entry(naclid, params)


#def bastion_revoke_access(instanceid, ipaddress=my_ip()):
#    ([sgid], naclid) = bastion_security_ids(instanceid)
#    cidr = '%s/32' % ipaddress
#    pprint(locals())
#
#    # Security Group
#    r = subtract_cidr_from_sg(sgid, CidrIp=cidr)
#
#    # NACL    if not already done.
#    Egress = False
#    try:
#        RuleNumber = nacl_rule_number_for_cidr(cidr, naclid, Egress)
#    except NoNaclEntry:
#        return
#    remove_nacl_entry(naclid, Egress, RuleNumber)
#
# Something in the two funcs above prevents completion of exec.


# Strangely, the above function defs cause the thing to hang.
print('fu ec2')
print('fu ec2')
# Strangely, the above function defs cause the thing to hang.


def bastion_info():
    try:
        # Find security group and NACL associated with the bastion instance so
        # we can alter the allowed IPs.

        iid = instanceids_in_autoscaling_group()[0]
        sgs = instance_security_groups(iid)
        sgid = sgs[0]
        public_ip = instance_ip(iid)
        iss = instance_subnet(iid)
        ivpc = instance_vpc(iid)

        nacl_dict = ec2.describe_network_acls(Filters = [{'Name': 'association.subnet-id',  'Values': [iss]}])['NetworkAcls'][0]
        naclid = nacl_dict['NetworkAclId']

    finally:
        print(iid)
        print(public_ip)
        print(sgs)
        print(sgid)
        print(naclid)
        print(iss)
        print(ivpc)
        sg_info(sgid)
        nacl_info(naclid)


def sg_permission_info(sgid):
    security_group = ec2r.SecurityGroup(sgid)
    decorate_methods(security_group, return_code_check)
    print('ingress')
    pprint(security_group.ip_permissions)
    print('egress')
    pprint(security_group.ip_permissions_egress)


def nacl_info(naclid):
    pprint(nacl_entries(naclid))


def open_egress_to_sg(sgid, CidrIp='0.0.0.0/0'):
    global security_group
    security_group = ec2r.SecurityGroup(sgid)
    decorate_methods(security_group, return_code_check)
    response = security_group.authorize_egress(IpPermissions=[
        dict(
        IpProtocol='tcp', 
        FromPort=-1, 
        ToPort=-1,
#        CidrIp=CidrIp
        )])
    return response


def add_cidr_to_sg(sgid, CidrIp='0.0.0.0/32'):
    security_group = ec2r.SecurityGroup(sgid)
    decorate_methods(security_group, return_code_check)
    try:
        response = security_group.authorize_ingress(
            CidrIp=CidrIp, IpProtocol='tcp', FromPort=22, ToPort=22,)
        return response
    except Exception as exc:
#        print(exc.args)
        return




# TODO:  nearly identical funcs 
def subtract_cidr_from_sg(sgid, CidrIp='8.8.8.8/32'):
    '''
    >>> subtract_thing('sg-b1cc31c7', CidrIp='8.8.8.8/32')
    '''
    security_group = ec2r.SecurityGroup(sgid)
    decorate_methods(security_group, return_code_check)
    try:
        response = security_group.revoke_ingress(
            CidrIp=CidrIp, IpProtocol='tcp', FromPort=22, ToPort=22,)
        return response
    except Exception as exc:
        return


# ################################# Subnets ################################# #


def describe_subnet(subnet_id):
    '''
    >>> [describe_subnet(private_subnetid_for_stack(stackid))['CidrBlock'] for stackid in substacks('foo', 'subnet')]
    ['10.0.5.0/24', '10.0.3.0/24']
    >>> [describe_subnet(public_subnetid_for_stack(stackid))['CidrBlock'] for stackid in substacks('foo', 'subnet')]
    ['10.0.4.0/24', '10.0.2.0/24']
    # TODO:  this docstring really belongs in stack/setup
    '''
    return ec2.describe_subnets(SubnetIds=[subnet_id])[0]  


def instanceids_in_autoscaling_groups():
    ''' [instance_ip(iid) for iid in instanceids_in_autoscaling_group()]
    '''
    asgs = autoscaling.describe_auto_scaling_groups()
    f = lambda asg: (asg['AutoScalingGroupName'], [d['InstanceId'] for d in asg['Instances']])
    try:
        return [f(asg) for asg in asgs]
    except IndexError:
        return


@remember
def describe_instance(instanceid):
    di = ec2.describe_instances(InstanceIds=[instanceid])[0]
    return di['Instances'][0]


def instance_ip(instanceid):
    return describe_instance(instanceid)['PublicIpAddress']

def instance_subnet(instanceid):
    return describe_instance(instanceid)['SubnetId']

def instance_vpc(instanceid):
    return describe_instance(instanceid)['VpcId']

def instance_security_groups(instanceid):
    result = [d['GroupId'] for d in describe_instance(instanceid)['SecurityGroups']]
    return result


def list_vpcs():
    return ec2.describe_vpcs()
#    return ec2.describe_vpcs()['Vpcs']
    '''
    Filters=[ { 'Name': 'string', 'Values': [ 'string', ] }, ],
    VpcIds=[ 'string', ],
    DryRun=True|False
    '''

def vpc_names():
    for vpc in list_vpcs():
        vpcid = vpc['VpcId']
        tags = vpc['Tags'] if 'Tags' in vpc else None
        print(vpcid)
#        print(tags, '\n')
        tags = tag_list_to_dict(tags) if tags else None
        owner = tags['Owner'] if tags and 'Owner' in tags else None
#        print(tags, '\n')
#        print(tags.keys(), '\n')
        print(owner, '\n')


def list_subnets(vpc_name=None):
    Filters = [{'Name': 'vpc-id', 'Values': [vpc_name]}] if vpc_name else []
    return ec2.describe_subnets(Filters=Filters)


def subnet_names(vpc_name=None):
    for subnet in list_subnets(vpc_name):
        sid = subnet['SubnetId']
        cidr = subnet['CidrBlock']
        vpc = subnet['VpcId'] 
        public = subnet['MapPublicIpOnLaunch'] 
        print(vpc, sid, cidr, public)
 
 
def public_subnets_for(vpc_name):
    func = lambda: list_subnets(vpc_name)
    transform = lambda dct: (dct['SubnetId'], dct['CidrBlock'])
    good = lambda dct: dct['MapPublicIpOnLaunch'] == True
    return some_stuff(func, good, transform)


def list_subnet_instances(subnet_name=None, transform=lambda d:d):

    Filters = [{'Name': 'subnet-id', 'Values': [ subnet_name ]}] if subnet_name else []
    result = ec2.describe_instances(Filters=Filters)
    if result:
        result = result[0]['Instances']
    return [transform(d) for d in result]


def subnet_instance_names(subnet_name=None, vpc_name=None):
    for subnet in list_subnets(vpc_name):
        sid = subnet['SubnetId']
        ins = list_subnet_instances(subnet_name=sid)
        print(sid, ins)
 
 
limits = '''
    Inbound or outbound rules per security groups == 50

    nacl limits:
        one nacl associated with 1 or more subnets.
        one nacl has max 20 ingress rules
        one nacl has max 20 egress rules
 
    '''

def amis():
    basic_amazon_linux = 'ami-1853ac65'
    return
    platform = 'linux'
    Filters = [{'Name': 'platform', 'Values': [ 'linux' ]}] 
    di = ec2.describe_images(Filters=Filters)
#    di = ec2.describe_images(Filters=Filters)['Images']
     

#di = ec2.describe_instances(InstanceIds=[iid])


def _debug_2018_03_18():
    '''The instances below are unable to fetch http pages.  Why?  Fix.
    '''
    dna = ec2.describe_network_acls()
    # list due to returning the value of the single key
    [dn.pop('Tags') for dn in dna]
    drt = ec2.describe_route_tables()
    [rt.pop('Tags') for rt in drt]
    for rt in drt: pass

    iids = ['i-04e0b0be316bf852f', 'i-0967a30b2927d5324']
    for instanceid in iids:
        [sgid] = instance_security_groups(instanceid)
        subnetid = instance_subnet(instanceid)
        ds = describe_subnet(subnetid)
        ds.pop('Tags')
        globals().update(locals())
        # check route table(s)
        # check subnet nacl
        for dn in dna:
            da = [d for d in dn['Associations'] if d['SubnetId'] == subnetid]
            if da:
                naclid = dn['NetworkAclId']
#                print(instanceid, sgid, subnetid, naclid, dn['VpcId'], '\n')
                nacl = dn
                [rt] = [rt for rt in drt if any([d for d in rt['Associations'] if 'SubnetId' in d and d['SubnetId']==subnetid])] 
                rtid = rt['RouteTableId']
#                print(instance_ip(instanceid), instanceid, sgid, subnetid, naclid, dn['VpcId'])
                print(instance_ip(instanceid), instanceid, sgid, subnetid, naclid, dn['VpcId'], rtid)
#            print(dn['NetworkAclId'], dn['VpcId'])

                print(rtid)
                pprint(rt)
                print(naclid)
                pprint(nacl)
        globals().update(locals())


def _debug():
    '''The instances below are unable to fetch http pages.  
    Why?
    SG only allowing 22 outbound.
    Fix template.
    '''
    iids = ['i-04e0b0be316bf852f', 'i-0967a30b2927d5324']
#               good                    old                     new
    iids = ['i-04e0b0be316bf852f', 'i-0967a30b2927d5324', 'i-0ff0b981dce0e4f2f']
    iids = ['i-04e0b0be316bf852f', 'i-0967a30b2927d5324', 'i-0cf4b807564acf7da']
    iids = ['i-04e0b0be316bf852f', 'i-0967a30b2927d5324', 'i-014a6407875881bc2']
    iids = ['i-04e0b0be316bf852f', 'i-0967a30b2927d5324', 'i-05fd7240ea4b6fbd2']


    sgids = flatten([instance_security_groups(iid) for iid in iids])
    dsgs = ec2.describe_security_groups(GroupIds=sgids)
    [sg.pop('Tags') for sg in dsgs]
    [sg.pop('OwnerId') for sg in dsgs]
    [sg.pop('VpcId') for sg in dsgs]
    [sg.pop('GroupName') for sg in dsgs]
    [sg.pop('Description') for sg in dsgs]
    common = extract_common_values(dsgs)
    parts = partition(dsgs, lambda sg:sg['GroupId'])

    for instanceid in iids:
        [sgid] = instance_security_groups(instanceid)
        print(instance_ip(instanceid), instanceid, sgid)
        pprint(parts[sgid])
        print('\n')
        globals().update(locals())
#    if len(dsg) == 1: dsg = dsg[0]



