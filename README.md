** https://bitbucket.org/carymiller/cm_cicd **

## Intro

Using boto3 to create, monitor, use and delete AWS resources.  The goal is to
create examples of working with the different AWS services to facilitate work in
real projects.  Current services include s3 (multipart uploads), ec2, iam, sqs,
firehose, ssm, resourcegroupstaggingapi and cloudformation.  Another goal is to
work on some of the less obvious aspects; things like paging.

The cloudformation code improves on the technique of including userdata scripts
in templates.  Including scripts in templates reduces maintainability and
severely impairs readability.  Code here keeps userdata and other scripts in
separate files.  Not only that, it constructs the userdata script by
concatenating scripts, which makes for good flexibility.

Also includes a complete use-case for the SSM service, updates arbitrary ec2
instances with a recent security update,  *security_update.py*

---

## Code Examples

1. list item
2. hyperlink [Clone a repository](https://confluence.atlassian.com/x/4whODQ)
3. *Italic*s
4. **bold**

---


